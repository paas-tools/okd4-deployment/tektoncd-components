# Tektoncd Components

Deploy TektonCD operator, pipelines and dashboard.

Pipelines are dependent on the tektoncd operator, but deployment won't succeed until the operator has been deployed first.
If this Helm chart is deployed with an ArgoCD Application, ArgoCD will retry deployment until it succeeds.

Toggle the dashboard deployment with the Helm value:

```yaml
dashboard:
    enabled: false
```

## Tekton Dashboard

The Tekton dashboard can't (yet) be installed on openshift with the Tekton operator.
Instead, we follow the upstream procedure to generate its openshift-compatible manifests for a specific release.
From the [tektoncd/dashboard repo](https://github.com/tektoncd/dashboard.git):

```yaml
scripts/installer --version v0.17.0 --openshift --namespace openshift-cern-tekton --pipelines-namespace tekton-pipelines --triggers-namespace tekton-pipelines --output tekton-dashboard.yaml
```

The installer script is copied here from upstream for convenience, since it fetches a specific version of the dashboard.

### Dashboard SSO

The dashboard should be protected behind SSO, but so far it's not integrated.
